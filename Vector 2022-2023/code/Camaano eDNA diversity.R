#load packages####
library(sdmTMB)
library(stringr)
library(tidyverse)
library(sf)
library(terra)
library(patchwork)
library(vegan)

#set plot theme####
source("../../functions/plt_plot_theme.R")
theme_set(plt_theme)

coast_line <- st_read("../../datasets/Coastline_low_res/BC-PacNW_BCAlbers.shp")

#list data####
south_CTD <- list.files("./Vector 2022-2023/data/2022/Rosette CTD/south/")
south_CTD <- south_CTD[south_CTD!="NOTES.txt"]
south_CTD <- south_CTD[!str_detect(south_CTD, "-salinity")] #remove salinity casts

north_CTD <- list.files("./Vector 2022-2023/data/2022/Rosette CTD/north/")
north_CTD <- north_CTD[north_CTD!="NOTES.txt"]
north_CTD <- north_CTD[!str_detect(north_CTD, "-salinity")] #remove salinity casts


sites <- read.csv("./Vector 2022-2023/data/2022/Rosette CTD/sites-casts.csv")
sites <- sites %>%
  filter(data.ok ==1) %>%
  mutate(site = ifelse(site == "n011-recast", "n11-recast", site)) %>%
  mutate(file = site) %>% 
  mutate(site = ifelse(site == "n02-salinity/n02-recast", "n02-recast", site)) %>%
  mutate(site = str_replace(site, "-recast", "")) %>% 
  filter(!str_detect(site, "-salinity")) %>%
  mutate(file = str_replace(file, "n02-salinity/", "")) %>% 
  arrange(site)

#process data
CTD_data <- data.frame()
for(i in south_CTD){
  print(i)
  site_data <- filter(sites, file == i)
  hold <- read.csv(paste("./Vector 2022-2023/data/2022/Rosette CTD/south/", i, "/",i,".csv", sep = ""))
  bottle_bottom <- hold %>% filter(bottles.fired == 1) %>% slice(1)
  bottle_surface <- hold %>% filter(bottles.fired> 0, pressure_db > 2.8, pressure_db < 3) %>% 
    summarise_all(mean)
  
  bottle_data <- bind_rows(bottle_bottom, bottle_surface) %>%  select(pressure_db, temperature_C = temperature_ITS.90_degC, conductivity_S.m, salinity_psu, fluorescence_mg.m.3, oxygen_ml.l) %>% 
    mutate(layer = c("bottom", "top"))
  
  CTD_data <- bind_rows(CTD_data, bind_cols(site_data %>% select(site, bottom_depth = vector.depth), bottle_data))
}
for(i in north_CTD){
  print(i)
  site_data <- filter(sites, file == i)
  hold <- read.csv(paste("./Vector 2022-2023/data/2022/Rosette CTD/north/", i, "/",i,".csv", sep = ""))
  bottle_bottom <- hold %>% filter(bottles.fired == 1) %>% slice(1)
  bottle_surface <- hold %>% filter(bottles.fired> 0, pressure_db > 2.8, pressure_db < 3) %>% 
    summarise_all(mean)
  
  bottle_data <- bind_rows(bottle_bottom, bottle_surface) %>%  select(pressure_db, temperature_C = temperature_ITS.90_degC, conductivity_S.m, salinity_psu, fluorescence_mg.m.3, oxygen_ml.l) %>% 
    mutate(layer = c("bottom", "top"))
  
  CTD_data <- bind_rows(CTD_data, bind_cols(site_data %>% select(site, bottom_depth = vector.depth), bottle_data))
}

CTD_data <- CTD_data %>% arrange(site)

data_log <- readxl::read_excel("./Vector 2022-2023/data/2022/PAC2022-036_VectorMay_eDNA_log.xlsx")
data_log <- data_log %>% filter(Purpose == "eDNA", Method == "Vector Rosette") %>% mutate(Site = tolower(Site)) %>% mutate(Site = str_replace_all(Site, "\\s", ""))

data_log <- data_log %>% 
  select(Site, Region, Lat_deg_dec, Lon_deg_dec, `Bottom Depth (m)`) %>% 
  filter(!Site %in% c("blank", "coral1", "coral2", "coral3", "sponge1", "sponge2", "sponge3")) %>% 
  unique() %>% 
  mutate(`Bottom Depth (m)` = as.numeric(`Bottom Depth (m)`)) %>% 
  st_as_sf(coords = c("Lon_deg_dec", "Lat_deg_dec"), crs = "EPSG:4326") %>% 
  st_transform(crs = "EPSG:3005") %>% 
  arrange(Site)

CTD_data <- left_join(data_log %>% select(site = Site), CTD_data)

#load eDNA####
fish_12S <- readxl::read_excel("./Vector 2022-2023/data/2022/Caamano_Sound_12S_fish.xlsx")
fish_16S <- readxl::read_excel("./Vector 2022-2023/data/2022/Caamano_Sound_16S_fish.xlsx")

read_threshold <- 5

fish_12S_long <- fish_12S %>% 
  gather(key = site, value = reads, -Family, -Genus, -Species) %>% 
  group_by(Family, Genus, Species, site) %>% 
  filter(Species != "dropped") %>% 
  summarise(reads = sum(reads)) %>% 
  mutate(present = reads>read_threshold) %>% 
  mutate(
    layer = str_match(site, "([A-Za-z]+\\d+)_([A-Za-z]+)")[, 3],
    site = str_to_lower(str_match(site, "([A-Za-z]+\\d+)_([A-Za-z]+)")[, 2])
  ) %>% 
  filter(!is.na(layer))

fish_16S_long <- fish_16S %>% 
  gather(key = site, value = reads, -Family, -Genus, -Species) %>% 
  group_by(Family, Genus, Species, site) %>% 
  filter(Species != "dropped") %>% 
  summarise(reads = sum(reads)) %>% 
  mutate(present = reads>read_threshold) %>% 
  mutate(
    layer = str_match(site, "([A-Za-z]+\\d+)_([A-Za-z]+)")[, 3],
    site = str_to_lower(str_match(site, "([A-Za-z]+\\d+)_([A-Za-z]+)")[, 2])
  ) %>% 
  filter(!is.na(layer))

unique(c(fish_16S_long$Family, fish_12S_long$Family))


present <- right_join(CTD_data, fish_12S_long %>% 
  select(site, layer, Species, present_12S = present) %>% 
  left_join(fish_16S_long %>% 
               select(site, layer, Species, present_16S = present)) %>% 
  mutate(present_12S = ifelse(is.na(present_12S), 0, present_12S)) %>% 
  mutate(present_16S = ifelse(is.na(present_16S), 0, present_16S)) %>% 
  mutate(present = present_12S + present_16S) %>% 
  mutate(present = ifelse(present == 2, 1, present)) %>% 
  select(-present_12S, -present_16S)) %>% 
  filter(!is.na(temperature_C))

present <- present %>% 
  group_by(Species) %>% 
  mutate(total = sum(present)) %>% 
  filter(total > 0)

present %>% st_drop_geometry() %>% select(Species, total) %>% unique() %>% arrange(total)

coast_line <- coast_line %>% st_crop(st_bbox(present) + c(-3000, -3000, 3000, 3000))

speciesV <- unique(present$Species)
common <- taxize::sci2comm(speciesV)
common <- unlist(common)
species_key <- left_join(data.frame(Species = speciesV), data.frame(Species = names(common), common_name = common))
species_key %>% filter(is.na(common_name))
species_key$common_name[is.na(species_key$common_name)] <- c("stout blacksmelt", "northern ronquil", "red brotula", "rosylip sculpin", "lingcod", "painted greenling", "ragfish", "longfin sculpin", "ribbon prickleback", "prowfish", "smallhead eelpout")
present <- left_join(present, species_key)

for(i in 1:length(speciesV)){
  ggplot(data = present %>% filter(Species == species_key$Species[i])) + 
    geom_sf(data = coast_line)+
    geom_sf(aes(color = factor(present, levels = c(0, 1))), size = 3)+
    scale_color_manual(values = c("black", "red"), "> 5 reads", drop = FALSE)+
    facet_wrap(~layer)+
    coord_sf(expand = FALSE)+
    ggtitle(paste(species_key$common_name[i], " (", species_key$Species[i], ")", sep = ""))+
    scale_x_continuous(breaks = seq(-129.4, -128.9, by = 0.2))
ggsave(paste("./Vector 2022-2023/figures/species_presence_maps/", species_key$common_name[i], ".png", sep = ""), height = 5, width = 7)
}

all_eDNA <- right_join(CTD_data, fish_12S_long %>% 
  mutate(type = "12S") %>% 
  bind_rows(fish_16S_long %>% mutate(type = "16S")))

for(i in 1:length(speciesV)){
  hold <- all_eDNA %>% filter(Species == species_key$Species[i])
  if(sum(hold$reads>0) > 1){
    ggplot(data = hold) + 
      geom_sf(data = coast_line)+
      geom_sf(aes(fill = reads), size = 3, pch = 21)+
      scale_fill_viridis_b(trans = "log10",na.value = "white")+
      #scale_fill_stepsn(colors = c("white", viridis::viridis(ceiling(log10(max(hold$reads, na.rm = TRUE))))), trans = "log10")+
      #scale_fill_stepsn(colors = viridis::viridis(ceiling(log10(max(hold$reads, na.rm = TRUE)))),breaks = 10^seq(0, ceiling(log10(max(hold$reads, na.rm = TRUE)))), na.value = "white")+
      facet_grid(type~layer)+
      coord_sf(expand = FALSE)+
      ggtitle(paste(species_key$common_name[i], " (", species_key$Species[i], ")", sep = ""))+
      scale_x_continuous(breaks = seq(-129.4, -128.9, by = 0.2))
  } else {
  ggplot(data = hold %>% mutate(reads = ifelse(reads == 0, NA, reads))) + 
    geom_sf(data = coast_line)+
    geom_sf(aes(fill = reads), size = 3, pch = 21)+
    scale_fill_viridis_c(na.value = "white")+
    #scale_fill_stepsn(colors = c("white", viridis::viridis(ceiling(log10(max(hold$reads, na.rm = TRUE))))), trans = "log10")+
    #scale_fill_stepsn(colors = viridis::viridis(ceiling(log10(max(hold$reads, na.rm = TRUE)))),breaks = 10^seq(0, ceiling(log10(max(hold$reads, na.rm = TRUE)))), na.value = "white")+
    facet_grid(type~layer)+
    coord_sf(expand = FALSE)+
    ggtitle(paste(species_key$common_name[i], " (", species_key$Species[i], ")", sep = ""))+
    scale_x_continuous(breaks = seq(-129.4, -128.9, by = 0.2))
  }
  ggsave(paste("./Vector 2022-2023/figures/species_reads_maps/", species_key$common_name[i], ".png", sep = ""), height = 10, width = 7)
}

alpha_div <- present %>% 
  group_by(site, layer, bottom_depth, pressure_db, temperature_C, salinity_psu, oxygen_ml.l) %>% 
  summarise(alpha_div = sum(present))

site_gamma <- present %>% 
  st_drop_geometry() %>% 
  group_by(site, Species) %>% 
  summarise(present = sum(present)) %>% 
  ungroup() %>% 
  group_by(site) %>% 
  summarise(gamma_site = sum(present>0))

beta_site <- left_join(alpha_div %>% group_by(site) %>% summarise(alpha_div = mean(alpha_div)), 
                       site_gamma) %>% 
  mutate(beta_site = gamma_site/alpha_div) %>% 
  left_join(alpha_div %>% st_drop_geometry() %>% filter(layer == "bottom") %>% select(-alpha_div))



(alpha_div %>% 
  ggplot()+
  geom_sf(data = coast_line) +
  geom_sf(size = 3, aes(color = alpha_div))+
  facet_wrap(~layer)+
  scale_color_viridis_c()+
  coord_sf(expand = FALSE)+
  scale_x_continuous(breaks = seq(-129.4, -128.9, by = 0.2)))

(beta_site %>% 
  ggplot()+
  geom_sf(data = coast_line) +
  geom_sf(size = 3, aes(color = gamma_site))+
  scale_color_viridis_c()+
  coord_sf(expand = FALSE)+
  scale_x_continuous(breaks = seq(-129.4, -128.9, by = 0.2))+
  
  beta_site %>% 
  ggplot()+
  geom_sf(data = coast_line) +
  geom_sf(size = 3, aes(color = beta_site))+
  scale_color_viridis_c()+
  coord_sf(expand = FALSE)+
  scale_x_continuous(breaks = seq(-129.4, -128.9, by = 0.2)))+

  plot_layout(nrow = 1)

alpha_div %>% 
  ggplot(aes(x = pressure_db, y = alpha_div, color = temperature_C))+
  geom_point()+
  scale_color_viridis_c()+
  facet_wrap(~layer, scales = "free_x")

alpha_div %>% 
  ggplot(aes(color = pressure_db, y = alpha_div, x = temperature_C))+
  geom_point(size = 2)+
  scale_color_viridis_c("depth m")+
  facet_wrap(~layer, scales = "free_x")

alpha_div %>% 
  ggplot(aes(color = pressure_db, y = alpha_div, x = oxygen_ml.l))+
  geom_point(size = 2)+
  scale_color_viridis_c("depth m")+
  facet_wrap(~layer, scales = "free_x")

beta_site %>% 
  ggplot(aes(x = pressure_db, y = beta_site))+
  geom_point(size = 2)+
  geom_smooth(method = "lm")+
  scale_color_viridis_c()+
  xlab("depth m")

present %>% 
  group_by(site, layer, bottom_depth, pressure_db, temperature_C, salinity_psu, oxygen_ml.l) %>% 
  summarise(alpha_div = sum(present)) %>% 
  ggplot(aes(x = layer, y = alpha_div, fill = layer))+
  geom_violin()+
  geom_jitter(width = 0.05)+
  theme_bw()

#nmds####
present_wide <- present %>% 
  st_drop_geometry() %>% 
  select(-Family, -Genus) %>% 
  group_by(Species) %>% 
  mutate(present_sum = sum(present)) %>% 
  filter(present_sum > 2) %>% 
  select(-present_sum) %>% 
  spread(key = Species, value = present, fill = 0)

nmds_presence <- metaMDS(present_wide[,-c(1:which(names(present_wide)=="layer"))], distance = "jaccard", k = 2)
plot(nmds_presence)
nmds_scores <- scores(nmds_presence)

present_wide %>% 
  select(site:layer) %>% 
  mutate(region = str_to_upper(str_sub(site, 1,1)), site_number = str_sub(site, 2,3)) %>% 
  bind_cols(nmds_scores$sites) %>% 
  ggplot(aes(x = NMDS1, y = NMDS2, color = bottom_depth))+
  geom_point(size = 7)+
  geom_text(aes(label = site_number), color  = "white") +
  facet_grid(region~layer)+
  scale_color_viridis_c("bottom\ndepth", direction = -1)
